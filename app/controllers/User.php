<?php

class User extends Controller {

    public function index() {
        $data["judul"] = "User";
        $this->view("templates/header", $data);
        $this->view("user/index");
        $this->view("templates/footer");
    }

    public function profile($nama = "Vira Septiari", $pekerjaan = "pelajar", $sekolah = "SMK Negeri 1 Denpasar") {
        $data["judul"] = "User";
        $data["nama"] = $nama;
        $data["pekerjaan"] = $pekerjaan;
        $data["sekolah"] = $sekolah;
        $this->view("templates/header", $data);
        $this->view("user/profile", $data);
        $this->view("templates/footer");
    }
}