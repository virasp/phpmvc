<?php

class Login extends Controller
{
    public function __construct()
    {
        if (isset($_SESSION['login'])) {
            header('location: ' . BASE_URL . '/home');
        }
    }
    public function index()
    {
        $data['judul'] = 'Login';
        $this->view('templates/header', $data);
        $this->view('login/index');
    }

    public function auth()
    {
        if ($this->model('User_model')->login($_POST) > 0) {
            $_SESSION['login'] = true;
            header('location: ' . BASE_URL . '/home');
        } else {
        }
    }

    public function logout()
    {
        session_destroy();
        header('Location: ' . BASE_URL . '/login');
        exit;
    }
}