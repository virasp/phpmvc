<div class="container my-5">
    <div class="d-flex justify-content-center m-2">
        <div class="col-xl-10 col-lg-12">
            <div class="card border-0 shadow-lg">
                <div class="card-bodyp-0">
                    <div class="row">
                        <div class="col-lg-5" style="background-image: url('<?= BASE_URL; ?>/img/profile.jpeg'); background-size:cover; background-position: center;"></div>
                        <div class="col-lg-6 p-5">
                            <div class="m-5">
                                <div class="mb-4 text-center">
                                    <h5>Login</h5>
                                </div>
                                <form action="<?= BASE_URL; ?>/login/auth" method="post">
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Username</label>
                                        <input type="text" class="form-control" placeholder="username" name="username">
                                    </div>
                                    <div class="form-group mb-3">
                                        <label for="" class="mb-2">Password</label>
                                        <input type="password" class="form-control" placeholder="password" name="password">
                                    </div>

                                    <div class="text-center">
                                        <button class="btn bg-danger text-light mb-3" style="width: 100%;">Login</button>
                                        <p>Dont't have an account? <a href="<?= BASE_URL; ?>/register" class="text-decoration-none text-danger">Register</a></p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
