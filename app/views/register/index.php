<div class="container pt-5">
  <div class="row mx-auto">
   <div class="col-12 col-sm-8 col-md-6 m-auto">
    <div class="card border border-black shadow-lg">
      <div class="card-body">
        <div class="text-center mt-3">
        </div>
        <h1 class="text-center">REGISTER</h1>
          <form action="<?= BASE_URL;?>/register/prosesRegister" method="post">
            <input type="text" class="form-control my-4 py-2" placeholder="Username" name="username">
            <input type="text" class="form-control my-4 py-2" placeholder="Email" name="email">
            <input type="password" class="form-control my-4 py-2" placeholder="Password" name="password">
            <input type="password" class="form-control my-4 py-2" placeholder="Confirm Password" name="cpassword">
            <div class="text-center mt-3">
              <button type="submit" class="btn btn-danger">Register</button>
              <p class="text-secondary mt-3">Have an account? <a href="<?= BASE_URL; ?>/login" class="text-decoration-none">Login</a></p>
            </div>
          </form>
      </div>

    </div>
   </div>
  </div>
</div>
